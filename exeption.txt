using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab
{
   
    class Program

    {
        static void Main(string[] args)
        {
            string[] names = { "Dog", "Cat", "Fish" };
            Object[] objs = (Object[])names;
            Console.WriteLine("Введите любое число: ");
            int s = int.Parse(Console.ReadLine());
            try
            {
                objs[2] = s;
                foreach (object animalName in objs)
                {
                    Console.WriteLine(animalName);
                }
            }1
            catch (ArrayTypeMismatchException)
            {
                Console.WriteLine("Не удалось преобразовать тип!");
            }
            int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine();
            try
            {
                arr[5] /= 0;
                Console.WriteLine(arr[5]);
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Делить на 0 нельзя!");
            }
            try
            {
                arr[13] += 13;
                Console.WriteLine(arr[3]);
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Нет такого элемента!");
            }
            bool bol = true;
            try
            {
                char temp = Convert.ToChar(bol);
            }
            catch (InvalidCastException)
            {
                Console.WriteLine("Невозможно конвертировать несовместимые типы!");
            }
            StringBuilder sb = new StringBuilder(15, 15);
            sb.Append("Машиностроение ");
            try
            {
                sb.Insert(0, "Трактор", 0);
            }
            catch (OutOfMemoryException)
            {
                Console.WriteLine("Не хватает выделенной памяти!!!!");
            }
            Console.WriteLine("Введите число больше 255: ");
            try
            {
                byte by = byte.Parse(Console.ReadLine());
            }
            catch (OverflowException ex)
            {
                Console.WriteLine("Произошло перенаполнение: " + ex.Message);
            }
            Console.ReadKey();
        }
    }
} 
